const withPWA = require("next-pwa");
const runtimeCaching = require("next-pwa/cache");

const securityHeaders = [
  {
    key: "X-DNS-Prefetch-Control",
    value: "on",
  },
  {
    key: "X-XSS-Protection",
    value: "1; mode=block",
  },
  {
    key: "X-Frame-Options",
    value: "SAMEORIGIN",
  },
];

/** @type {import('next').NextConfig} */
module.exports = withPWA({
  reactStrictMode: true,
  images: {
    domains: ["chillhop.com"],
  },
  pwa: {
    dest: "public",
    runtimeCaching,
  },
  async headers() {
    return [
      {
        // security headers for all routes
        source: "/(.*)",
        headers: securityHeaders,
      },
    ];
  },
});
