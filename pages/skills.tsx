import type { PageProps } from "../utils/types";
import dynamicPage from "../utils/dynamicPage";

const SkillsMobile = dynamicPage(() => import("../components/Mobile/Skills"));
const SkillsDesktop = dynamicPage(() => import("../components/Desktop/Skills"));

export default function SkillsPage({ displayMobileView }: PageProps) {
  return displayMobileView ? <SkillsMobile /> : <SkillsDesktop />;
}
