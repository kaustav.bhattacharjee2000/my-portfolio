import { Text } from "@chakra-ui/react";

import type { PageProps } from "../utils/types";
import MiscPage from "../components/Common/MiscPage";

export default function Offline({ displayMobileView }: PageProps) {
  return (
    <MiscPage displayMobileView={displayMobileView} title="Offline">
      <Text align="center">
        {`Oops! It seems that you're offline right now. Please refresh the page when you're online.}`}
      </Text>
    </MiscPage>
  );
}
