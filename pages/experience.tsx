import React from "react";
import {
  Box,
  Flex,
  VStack,
  Text,
  Link,
  Heading,
  Spacer,
  Center,
  Button,
  useColorModeValue,
} from "@chakra-ui/react";

import type { ExperienceCardProps, PageProps } from "../utils/types";
import ScreenTitle from "../components/Common/ScreenTitle";
import { data } from "../utils/data";

function ExperienceCard(props: ExperienceCardProps) {
  const bgColor = useColorModeValue("white", "gray.700");
  const borderWidth = useColorModeValue("1px", "0");
  const headingColor = useColorModeValue("inherit", "white");
  const visitBtnBg = useColorModeValue("red.300", "gray.500");
  const visitBtnHoverBg = useColorModeValue("red.400", "gray.600");

  return (
    <Box
      w="full"
      rounded="xl"
      px={props.isMobile ? 3 : 6}
      py="4"
      bg={bgColor}
      border={borderWidth}
      borderColor="gray.200"
    >
      <Heading color={headingColor} size={props.isMobile ? "sm" : "md"}>
        {props.position}
      </Heading>

      <Flex mt="2" alignItems="center">
        <Text color={"red.300"}>{props.company}</Text>
        <Spacer />
        <Text fontSize="sm">{`${props.timeline[0]} to ${props.timeline[1]}`}</Text>
      </Flex>

      <Text mt="3" align="justify" fontSize={props.isMobile ? 14 : "initial"}>
        {props.description}
      </Text>

      <Link href={props.website} _hover={{ textDecoration: "none" }} isExternal>
        <Button
          mt="4"
          size="sm"
          color="white"
          bg={visitBtnBg}
          _hover={{
            bg: visitBtnHoverBg,
          }}
        >
          Visit website
        </Button>
      </Link>
    </Box>
  );
}

export default function ExperiencePage({ displayMobileView }: PageProps) {
  const textColor = useColorModeValue("gray.600", "whiteAlpha.700");
  const pt = displayMobileView ? 2 : 8;

  return (
    <Center w="full">
      <VStack
        d="flex"
        flexDir="column"
        flex="1"
        color={textColor}
        zIndex={50}
        px="4"
        pt={pt}
        pb="2"
        spacing="8"
        mb="8"
      >
        <ScreenTitle
          title={displayMobileView ? "Experiences" : "My work history"}
          noToggle={!displayMobileView}
        />
        <VStack spacing="8">
          {data.experiences.map((details, index) => (
            <ExperienceCard
              key={`exp-${index}`}
              isMobile={displayMobileView}
              {...details}
            />
          ))}
        </VStack>
      </VStack>
    </Center>
  );
}
