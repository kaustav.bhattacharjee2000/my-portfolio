import type { PageProps } from "../utils/types";
import dynamicPage from "../utils/dynamicPage";

const HomeMobile = dynamicPage(() => import("../components/Mobile/Home"));
const HomeDesktop = dynamicPage(() => import("../components/Desktop/Home"));

export default function IndexPage({ displayMobileView }: PageProps) {
  return displayMobileView ? <HomeMobile /> : <HomeDesktop />;
}
