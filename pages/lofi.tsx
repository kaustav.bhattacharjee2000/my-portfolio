import React, { Fragment } from "react";
import Head from "next/head";

import type { PageProps } from "../utils/types";
import dynamicPage from "../utils/dynamicPage";
import MusicPlayer from "../components/Common/MusicPlayer";

const LofiMobile = dynamicPage(() => import("../components/Mobile/Lofi"));
const LofiDesktop = dynamicPage(() => import("../components/Desktop/Lofi"));

export default function LofiPage({ displayMobileView }: PageProps) {
  return (
    <Fragment>
      <Head>
        <link rel="prefetch" href="https://mp3.chillhop.com/serve.php" />
        <link rel="dns-prefetch" href="https://mp3.chillhop.com/serve.php" />
      </Head>

      <MusicPlayer page={displayMobileView ? LofiMobile : LofiDesktop} />
    </Fragment>
  );
}
