import type { PageProps } from "../utils/types";
import dynamicPage from "../utils/dynamicPage";

const ProjectsMobile = dynamicPage(
  () => import("../components/Mobile/Projects")
);
const ProjectsDesktop = dynamicPage(
  () => import("../components/Desktop/Projects")
);

export default function ProjectsPage({ displayMobileView }: PageProps) {
  return displayMobileView ? <ProjectsMobile /> : <ProjectsDesktop />;
}
