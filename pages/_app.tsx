import "@fontsource/lato";
import type { AppProps } from "next/app";
import Head from "next/head";
import { ChakraProvider } from "@chakra-ui/react";

import BaseLayout from "../components/Common/BaseLayout";
import theme from "../utils/theme";

export default function KaustavApp({ Component, pageProps, router }: AppProps) {
  return (
    <ChakraProvider theme={theme}>
      <Head>
        <title>{"Kaustav's Portfolio"}</title>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=5.0, minimum-scale=1.0"
        />
      </Head>

      <BaseLayout page={Component} {...pageProps} />
    </ChakraProvider>
  );
}
