import { Text } from "@chakra-ui/react";

import type { PageProps } from "../utils/types";
import MiscPage from "../components/Common/MiscPage";

export default function UnexpectedErrorPage({ displayMobileView }: PageProps) {
  return (
    <MiscPage displayMobileView={displayMobileView} title="Error">
      <Text align="center">
        {`Oops! An unexpected error as occured. Please refresh the page or try 
        refreshing after some time if this page shows up again.`}
      </Text>
    </MiscPage>
  );
}
