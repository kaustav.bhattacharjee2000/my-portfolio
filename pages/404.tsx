import { Text } from "@chakra-ui/react";

import type { PageProps } from "../utils/types";
import MiscPage from "../components/Common/MiscPage";

export default function NotFoundPage({ displayMobileView }: PageProps) {
  return (
    <MiscPage displayMobileView={displayMobileView} title="404 Not Found">
      <Text align="center">
        {`Oops! It seems that the page you wanted to visit couldn't be found.
        Check the url again or try one of the links ${
          displayMobileView ? "at the bottom." : "on the left."
        }`}
      </Text>
    </MiscPage>
  );
}
