import { randomBytes } from "crypto";
import { ColorModeScript } from "@chakra-ui/react";
import NextDocument, { Html, Head, Main, NextScript } from "next/document";

const cspRules__DEV = {
  "default-src": ["'self'"],
  "base-uri": ["'self'"],
  "img-src": ["'self'", "chillhop.com", "i.scdn.co", "data:"],
  "media-src": ["'unsafe-inline'", "mp3.chillhop.com", "data:"],
  "script-src": ["'self'", "'unsafe-eval'", "'unsafe-inline'"],
  "style-src": ["'self'", "'unsafe-inline'"],
  "connect-src": ["'self'", "mp3.chillhop.com"],
  "prefetch-src": ["'self'", "mp3.chillhop.com"],
  "object-src": ["'self'", "data:"],
};

const cspRules__PROD = {
  ...cspRules__DEV,
  "script-src": ["'self'", "'unsafe-inline'"],
};

const generateNonce = () => {
  return `${randomBytes(16).toString("base64")}`;
};

const generateCSP = () => {
  let nonce = generateNonce();
  let cspObject =
    process.env.NODE_ENV === "development" ? cspRules__DEV : cspRules__PROD;

  cspObject["script-src"].push(`'nonce-${nonce}'`);

  let csp = Object.entries(cspObject)
    .map(([k, v]) => k + " " + v.join(" ") + ";")
    .join(" ");

  return [csp, nonce];
};

export default class Document extends NextDocument {
  render() {
    let [csp, nonce] = generateCSP();

    return (
      <Html lang="en" dir="ltr">
        <Head nonce={nonce}>
          <meta property="csp-nonce" content={nonce} />
          <meta httpEquiv="Content-Security-Policy" content={`${csp}`} />
          <meta name="description" content="Kaustav's Portfolio" />
          <meta
            name="keywords"
            content="portfolio, sharing, skills, experience, projects, lofi, music"
          />
          <meta name="theme-color" content="#FC8181" />
          <meta name="mobile-web-app-capable" content="yes" />
          <meta name="apple-mobile-web-app-title" content="Portfolio" />
          <meta name="apple-mobile-web-app-capable" content="yes" />
          <meta
            name="apple-mobile-web-app-status-bar-style"
            content="#FC8181"
          />
          <meta name="format-detection" content="telephone=no" />
          <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
          <link
            rel="icon"
            href="/icons/favicon_16x16.png"
            sizes="16x16"
            type="image/png"
          />
          <link
            rel="icon"
            href="/icons/favicon_32x32.png"
            sizes="32x32"
            type="image/png"
          />
          <link
            rel="icon"
            href="/icons/favicon_48x48.png"
            sizes="48x48"
            type="image/png"
          />
          <link
            rel="icon"
            href="/icons/favicon_96x96.png"
            sizes="96x96"
            type="image/png"
          />
          <link
            rel="icon"
            href="/icons/favicon_144x144.png"
            sizes="144x144"
            type="image/png"
          />
          <link
            rel="icon"
            href="/icons/favicon_192x192.png"
            sizes="192x192"
            type="image/png"
          />
          <link
            rel="icon"
            href="/icons/favicon_256x256.png"
            sizes="256x256"
            type="image/png"
          />
          <link
            rel="icon"
            href="/icons/favicon_512x512.png"
            sizes="512x512"
            type="image/png"
          />
          <link
            rel="apple-touch-icon"
            href="/icons/apple-touch-icon.png"
            sizes="180x180"
          />
          <link
            rel="apple-touch-startup-image"
            href="/apple_splashscreens/iphoneplus_splash.png"
            media="(device-width: 621px) and (device-height: 1104px) and (-webkit-device-pixel-ratio: 3)"
          />
          <link
            rel="apple-touch-startup-image"
            href="/apple_splashscreens/iphonex_splash.png"
            media="(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3)"
          />
          <link
            rel="apple-touch-startup-image"
            href="/apple_splashscreens/iphonexsmax_splash.png"
            media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3)"
          />
          <link
            rel="apple-touch-startup-image"
            href="/apple_splashscreens/ipad_splash.png"
            media="(device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2)"
          />
          <link
            rel="apple-touch-startup-image"
            href="/apple_splashscreens/ipadpro1_splash.png"
            media="(device-width: 834px) and (device-height: 1112px) and (-webkit-device-pixel-ratio: 2)"
          />
          <link
            rel="apple-touch-startup-image"
            href="/apple_splashscreens/ipadpro3_splash.png"
            media="(device-width: 834px) and (device-height: 1194px) and (-webkit-device-pixel-ratio: 2)"
          />
          <link
            rel="apple-touch-startup-image"
            href="/apple_splashscreens/ipadpro2_splash.png"
            media="(device-width: 1024px) and (device-height: 1366px) and (-webkit-device-pixel-ratio: 2)"
          />
          <link
            rel="apple-touch-startup-image"
            href="/apple_splashscreens/iphone5_splash.png"
            media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)"
          />
          <link
            rel="apple-touch-startup-image"
            href="/apple_splashscreens/iphone6_splash.png"
            media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)"
          />
          <link
            rel="apple-touch-startup-image"
            href="/apple_splashscreens/iphonexr_splash.png"
            media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2)"
          />
          <link rel="manifest" href="/manifest.json" />
          <meta property="og:image" content="/icons/favicon_260x260.png" />
          <meta property="og:image:type" content="image/png" />
          <meta property="og:image:width" content="260" />
          <meta property="og:image:height" content="260" />
          <meta property="og:image:alt" content="icon" />
          <meta property="og:title" content="Kaustav's Portfolio" />
          <meta property="og:url" content="https://kaustav.vercel.app" />
          <meta property="og:type" content="website" />
        </Head>

        <body>
          <ColorModeScript nonce={nonce} />
          <Main />
          <NextScript nonce={nonce} />
        </body>
      </Html>
    );
  }
}
