import React from "react";
import { VStack, Flex, Center, useColorModeValue } from "@chakra-ui/react";

import ProjectCard from "../Common/ProjectCard";
import ScreenTitle from "../Common/ScreenTitle";
import { data } from "../../utils/data";

export default function Projects() {
  const textColor = useColorModeValue("gray.600", "whiteAlpha.700");

  let firstColumn = data.projects.filter((_, id) => id % 2 === 0);
  let secondColumn = data.projects.filter((_, id) => id % 2 === 1);

  return (
    <Center w="full">
      <VStack
        d="flex"
        flexDir="column"
        flex="1"
        color={textColor}
        px="4"
        pt="8"
        pb="2"
        maxW="900px"
      >
        <ScreenTitle title="Projects I made" noToggle />
        <Flex w="full" alignItems="start" justifyContent="space-evenly">
          {[firstColumn, secondColumn].map((column) => (
            <VStack key={`column-${column}`}>
              {column.map((project, index) => (
                <ProjectCard key={`project-${index}`} project={project} />
              ))}
            </VStack>
          ))}
        </Flex>
      </VStack>
    </Center>
  );
}
