import React from "react";
import NextImage from "next/image";
import {
  Center,
  Flex,
  Text,
  Box,
  Heading,
  VStack,
  Icon,
  Slider,
  SliderTrack,
  SliderFilledTrack,
  SliderThumb,
  Image,
  Square,
  keyframes,
  useColorModeValue,
  usePrefersReducedMotion,
} from "@chakra-ui/react";

import { FaPlay, FaForward, FaBackward } from "react-icons/fa";
import { IoPause } from "react-icons/io5";
import { MdGraphicEq } from "react-icons/md";

import type {
  LofiScreenProps,
  AudioControlsProps,
  PlaybackSliderProps,
} from "../../utils/types";

import lofiImage from "../../public/assets/lofi.jpg";

function LofiBanner() {
  return (
    <Box
      w="full"
      d="flex"
      flexDir="column"
      justifyContent="flex-end"
      h={"210px"}
      mb={6}
      pos={"relative"}
      color="white"
    >
      <Box w="full" h="full" overflow="hidden" rounded="xl">
        <NextImage src={lofiImage} alt="lofi background" objectFit="contain" />
      </Box>

      <Flex pos="absolute" b="0" l="0" ml={12} mb={10}>
        <VStack>
          <Heading>Lo-Fi Section</Heading>
          <Text>Random lo-fi songs to chill out !</Text>
        </VStack>
      </Flex>
    </Box>
  );
}

function AudioControls(props: AudioControlsProps) {
  return (
    <Flex mt="4" w="50%" justify="space-between" align="center">
      <Icon
        as={FaBackward}
        w="6"
        h="6"
        cursor="pointer"
        onClick={props.onPrevClick}
      />
      {props.isPlaying ? (
        <Icon
          as={IoPause}
          w="10"
          h="10"
          cursor="pointer"
          onClick={() => props.onPlayPauseClick(false)}
        />
      ) : (
        <Icon
          as={FaPlay}
          w="10"
          h="10"
          cursor="pointer"
          onClick={() => props.onPlayPauseClick(true)}
        />
      )}
      <Icon
        as={FaForward}
        w="6"
        h="6"
        cursor="pointer"
        onClick={props.onNextClick}
      />
    </Flex>
  );
}

function PlaybackSlider(props: PlaybackSliderProps) {
  const { spentTime, totalTime } = props.audioTimes();

  return (
    <Flex mt="6" w="full" justify="space-between" align="center">
      <Text fontSize="sm" w="12" align="start">
        {spentTime}
      </Text>

      <Slider
        flex="1"
        aria-label="playback-slider"
        defaultValue={0}
        min={0}
        step={1}
        max={props.duration ? props.duration : 0}
        value={props.trackProgress}
        onChange={(value) => props.onScrub(value)}
      >
        <SliderTrack bg="red.100">
          <SliderFilledTrack bg="gray.300" />
        </SliderTrack>
        <SliderThumb boxSize={6}>
          <Box color="tomato" as={MdGraphicEq} />
        </SliderThumb>
      </Slider>

      <Text fontSize="sm" w="12" align="end">
        {totalTime}
      </Text>
    </Flex>
  );
}

const animatedGradient = keyframes`
  0% { background-position: 0% 50%; }
  50% { background-position: 100% 50%; }
  100% { background-position: 0% 50%; }
`;

export default function LofiPage(props: LofiScreenProps) {
  const textColor = useColorModeValue("gray.700", "gray.300");
  const prefersReducedMotion = usePrefersReducedMotion();

  const playbackControlsAnimation = prefersReducedMotion
    ? undefined
    : `${animatedGradient} 10s ease infinite`;

  return (
    <Center w="full" color={textColor}>
      <Flex flexDir="column" flex="1" maxW="900px" py="10">
        <LofiBanner />

        <Heading color="red.300" fontSize="24" mt="4">
          Now playing
        </Heading>

        <Flex mt="6" w="full" align="center">
          <VStack spacing="4">
            <Square rounded="xl" overflow="hidden" size="44">
              <Image
                src={props.track.cover}
                alt={`${props.track.name} cover`}
              />
            </Square>

            <VStack maxW="56">
              <Center>
                <Heading size="md" isTruncated>
                  {props.track.name}
                </Heading>
              </Center>
              <Center>
                <Text isTruncated>{props.track.artist}</Text>
              </Center>
            </VStack>
          </VStack>

          <Flex
            flexDir="column"
            justify="center"
            align="center"
            h="max-content"
            ml="8"
            flex="1"
            py="4"
            px="8"
            rounded="2xl"
            background="linear-gradient(320deg, #ee5f34, #44d1e8)"
            backgroundSize="400% 400%"
            color="white"
            animation={playbackControlsAnimation}
          >
            <AudioControls {...props.audioControls} />
            <PlaybackSlider {...props.playbackSlider} />
          </Flex>
        </Flex>
      </Flex>
    </Center>
  );
}
