import React from "react";
import NextImage from "next/image";
import {
  Box,
  VStack,
  Heading,
  Tag,
  Center,
  SimpleGrid,
  useColorModeValue,
} from "@chakra-ui/react";

import type { Skill } from "../../utils/types";
import ScreenTitle from "../Common/ScreenTitle";
import { data } from "../../utils/data";

import frontendImage from "../../public/external/Front-End.jpg";
import backendImage from "../../public/external/Back-End.jpg";
import dbImage from "../../public/external/Database.jpg";
import mobileImage from "../../public/external/Mobile.jpg";
import mlImage from "../../public/external/Machine-Learning.jpg";
import othersImage from "../../public/external/Others.jpg";

interface SkillCardProps {
  skill: Skill;
}

const skillImageMapping: Record<string, StaticImageData> = {
  "/external/Front-End.jpg": frontendImage,
  "/external/Back-End.jpg": backendImage,
  "/external/Database.jpg": dbImage,
  "/external/Mobile.jpg": mobileImage,
  "/external/Machine-Learning.jpg": mlImage,
  "/external/Others.jpg": othersImage,
};

function SkillCard(props: SkillCardProps) {
  const bg = useColorModeValue("white", "gray.700");
  const headingTextColor = useColorModeValue("gray.700", "white");

  return (
    <Center py={6}>
      <Box
        maxW={"300px"}
        h="full"
        w={"full"}
        bg={bg}
        boxShadow={"2xl"}
        rounded={"md"}
        p={6}
        overflow={"hidden"}
      >
        <Box
          h={"210px"}
          bg={"gray.100"}
          mt={-6}
          mx={-6}
          mb={6}
          pos={"relative"}
        >
          <NextImage
            src={skillImageMapping[props.skill.image]}
            layout={"fill"}
            objectFit="cover"
            alt={`${props.skill.title} image`}
          />
        </Box>

        <VStack alignItems="start">
          <Center w="full">
            <Heading
              color={headingTextColor}
              fontSize={"2xl"}
              fontFamily={"body"}
            >
              {props.skill.title}
            </Heading>
          </Center>

          <Box alignItems="start">
            {props.skill.tags.map((tagName, index) => (
              <Tag key={`tag-${props.skill.title}-${index}`} mx="1" my="2">
                {tagName}
              </Tag>
            ))}
          </Box>
        </VStack>
      </Box>
    </Center>
  );
}

export default function Skills() {
  const textColor = useColorModeValue("gray.600", "whiteAlpha.700");

  return (
    <Center w="full">
      <VStack
        d="flex"
        flexDir="column"
        flex="1"
        color={textColor}
        px="4"
        pt="8"
        pb="2"
        maxW="900px"
      >
        <ScreenTitle title="Skills I have" noToggle />
        <SimpleGrid columns={3} spacing={8}>
          {data.skills.map((skill, index) => (
            <SkillCard skill={skill} key={`skill-${index}`} />
          ))}
        </SimpleGrid>
      </VStack>
    </Center>
  );
}
