import React from "react";
import NextImage from "next/image";
import {
  Text,
  Box,
  Flex,
  Heading,
  Circle,
  Center,
  Link,
  Image,
  Icon,
  Square,
  Spacer,
  Button,
  SimpleGrid,
  useClipboard,
  useColorModeValue,
} from "@chakra-ui/react";

import type { LinkBoxProps } from "../../utils/types";
import { data } from "../../utils/data";
import bgImage from "../../public/external/bg.jpg";

function LinkBox(props: LinkBoxProps) {
  const { hasCopied, onCopy } = useClipboard(props.url);
  const borderColor = useColorModeValue("gray.300", "inherit");
  const iconWrapperBg = useColorModeValue(
    props.iconBgColor[props.iconBgColor.length - 1],
    props.iconBgColor[0]
  );
  const iconColor = useColorModeValue(
    props.iconColor[props.iconColor.length - 1],
    props.iconColor[0]
  );

  return (
    <Flex
      border="1px"
      borderColor={borderColor}
      px="6"
      py="4"
      rounded="xl"
      alignItems="center"
    >
      <Link
        href={props.url}
        textDecoration="none"
        aria-label={props.title}
        isExternal
      >
        <Square bg={iconWrapperBg} size="40px" rounded="lg">
          <Icon
            as={props.icon}
            w={props.iconSize}
            h={props.iconSize}
            color={iconColor}
          />
        </Square>
      </Link>

      <Box flex="1" ml="4">
        <Flex alignItems="center">
          <Text fontWeight="bold">{props.title}</Text>

          <Spacer />

          <Button size="sm" onClick={onCopy}>
            {hasCopied ? "Copied" : "Copy"}
          </Button>
        </Flex>

        <Text fontSize="14" isTruncated>
          {props.subtitle}
        </Text>
      </Box>
    </Flex>
  );
}

function Banner() {
  return (
    <Box
      w="full"
      d="flex"
      flexDir="column"
      justifyContent="flex-end"
      h={"210px"}
      mb={6}
      pos={"relative"}
    >
      <Box w="full" h="full" overflow="hidden" rounded="xl">
        <NextImage src={bgImage} alt="profile background" layout="responsive" />
      </Box>

      <Flex pos="absolute" b="0" l="0" ml={12} mb="-8">
        <Circle
          d="flex"
          alignItems="flex-end"
          size="36"
          border="2px"
          borderColor="gray.300"
          overflow="hidden"
        >
          <Image src={data.profilePic} alt="My picture" />
        </Circle>

        <Center ml={4} color="white" zIndex={123}>
          <Flex flexDir="column" align="center">
            <Heading>Kaustav Bhattacharjee</Heading>
            <Text>Software Developer</Text>
          </Flex>
        </Center>
      </Flex>
    </Box>
  );
}

export default function Home() {
  return (
    <Center w="full">
      <Flex flexDir="column" flex="1" py="10">
        <Banner />

        <Text mt="8" px="8" fontSize="xl" fontWeight="bold" color="red.300">
          Hi there !
        </Text>

        <Text px="8" fontSize="lg" align="justify">
          {data.aboutMe}
        </Text>

        <SimpleGrid columns={2} mt="12" px="8" mb="8" spacing={8}>
          {data.links.map((link, index) => (
            <LinkBox key={`mylink-${index}`} {...link} />
          ))}
        </SimpleGrid>

        <Center mt="8">
          <Link
            colorScheme="whiteAlpha"
            href={data.resume}
            _hover={{ textDecoration: "none" }}
            isExternal
          >
            <Button color="white" bg="red.400" _hover={{ bg: "red.300" }}>
              Download my resume
            </Button>
          </Link>
        </Center>
      </Flex>
    </Center>
  );
}
