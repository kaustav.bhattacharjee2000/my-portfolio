import React from "react";
import {
  Center,
  VStack,
  Flex,
  Text,
  Box,
  Heading,
  Image,
  Slider,
  SliderTrack,
  SliderFilledTrack,
  SliderThumb,
  Icon,
  keyframes,
  useColorModeValue,
  usePrefersReducedMotion,
} from "@chakra-ui/react";

import { FaPlay, FaForward, FaBackward } from "react-icons/fa";
import { IoPause } from "react-icons/io5";
import { MdGraphicEq } from "react-icons/md";

import type {
  AudioControlsProps,
  LofiScreenProps,
  PlaybackSliderProps,
} from "../../utils/types";

import ScreenTitle from "../Common/ScreenTitle";

function AudioControls(props: AudioControlsProps) {
  return (
    <Flex mt="4" w="75%" justify="space-between" align="center">
      <Icon
        as={FaBackward}
        w="6"
        h="6"
        cursor="pointer"
        onClick={props.onPrevClick}
      />
      {props.isPlaying ? (
        <Icon
          as={IoPause}
          w="10"
          h="10"
          cursor="pointer"
          onClick={() => props.onPlayPauseClick(false)}
        />
      ) : (
        <Icon
          as={FaPlay}
          w="10"
          h="10"
          cursor="pointer"
          onClick={() => props.onPlayPauseClick(true)}
        />
      )}
      <Icon
        as={FaForward}
        w="6"
        h="6"
        cursor="pointer"
        onClick={props.onNextClick}
      />
    </Flex>
  );
}

function PlaybackSlider(props: PlaybackSliderProps) {
  const { remainingTime } = props.audioTimes();

  return (
    <Flex mt="6" w="full" justify="space-between" align="center">
      <Slider
        ml={4}
        flex="1"
        aria-label="playback-slider"
        defaultValue={0}
        min={0}
        step={1}
        max={props.duration ? props.duration : 0}
        value={props.trackProgress}
        onChange={(value) => props.onScrub(value)}
      >
        <SliderTrack bg="red.100">
          <SliderFilledTrack bg="tomato" />
        </SliderTrack>
        <SliderThumb boxSize={6}>
          <Box color="tomato" as={MdGraphicEq} />
        </SliderThumb>
      </Slider>

      <Text fontSize="sm" w="12" align="end">
        {remainingTime}
      </Text>
    </Flex>
  );
}

const animatedGradient = keyframes`
  0% { background-position: 0% 50%; }
  50% { background-position: 100% 50%; }
  100% { background-position: 0% 50%; }
`;

export default function LofiPage(props: LofiScreenProps) {
  const textColor = useColorModeValue("gray.600", "whiteAlpha.700");
  const prefersReducedMotion = usePrefersReducedMotion();

  const playbackControlsAnimation = prefersReducedMotion
    ? undefined
    : `${animatedGradient} 10s ease infinite`;

  return (
    <VStack
      d="flex"
      flexDir="column"
      flex="1"
      color={textColor}
      zIndex={50}
      px="4"
      py="2"
      spacing="4"
    >
      <ScreenTitle title="Lo-Fi Section" />

      <Box w="full">
        <Center>
          <Text>Random lo-fi songs to chill out !</Text>
        </Center>

        <Center w="full" mt="4">
          <Flex
            flexDir="column"
            align="center"
            py="6"
            px="4"
            rounded="lg"
            w="full"
            maxW="300px"
            background="linear-gradient(320deg, #ee5f34, #44d1e8)"
            backgroundSize="400% 400%"
            color="white"
            boxShadow="0 28px 28px rgba(0, 0, 0, 0.2)"
            animation={playbackControlsAnimation}
          >
            <Image
              w="40"
              h="40"
              src={props.track.cover}
              alt={`${props.track.name} cover`}
              rounded="full"
            />

            <Heading mt="4">{props.track.name}</Heading>

            <Center mt="1" mb="2" maxW="260px">
              <Text isTruncated>{props.track.artist}</Text>
            </Center>

            <AudioControls {...props.audioControls} />

            <PlaybackSlider {...props.playbackSlider} />
          </Flex>
        </Center>
      </Box>
    </VStack>
  );
}
