import React from "react";
import { VStack, useColorModeValue } from "@chakra-ui/react";

import ProjectCard from "../Common/ProjectCard";
import ScreenTitle from "../Common/ScreenTitle";
import { data } from "../../utils/data";

export default function Projects() {
  const textColor = useColorModeValue("gray.600", "whiteAlpha.700");

  return (
    <VStack
      d="flex"
      flexDir="column"
      flex="1"
      color={textColor}
      zIndex={50}
      px="4"
      py="2"
      spacing="8"
      mb="8"
    >
      <ScreenTitle title="Projects" />
      <VStack spacing="8">
        {data.projects.map((project, index) => (
          <ProjectCard key={`project-${index}`} project={project} isMobile />
        ))}
      </VStack>
    </VStack>
  );
}
