import React from "react";
import {
  Box,
  VStack,
  Heading,
  HStack,
  Square,
  Icon,
  Tag,
  useColorModeValue,
} from "@chakra-ui/react";

import type { Skill } from "../../utils/types";
import ScreenTitle from "../Common/ScreenTitle";
import { data } from "../../utils/data";

interface SkillCardProps {
  skill: Skill;
}

function SkillCard(props: SkillCardProps) {
  const bgColor = useColorModeValue("white", "gray.600");
  const borderColor = useColorModeValue("1px", "0");
  const iconWrapperBg = useColorModeValue("red.300", "white");
  const iconColor = useColorModeValue("white", "red.300");

  return (
    <VStack
      w="full"
      bg={bgColor}
      border={borderColor}
      borderColor="gray.200"
      p="2"
      rounded="xl"
    >
      <HStack w="full" px="1">
        <Square bg={iconWrapperBg} p="1.5" rounded="lg">
          <Icon w="5" h="5" as={props.skill.icon} color={iconColor} />
        </Square>
        <Heading size="sm">{props.skill.title}</Heading>
      </HStack>

      <Box w="full">
        {props.skill.tags.map((tagName, index) => (
          <Tag key={`tag-${props.skill.title}-${index}`} m="1">
            {tagName}
          </Tag>
        ))}
      </Box>
    </VStack>
  );
}

export default function Skills() {
  const textColor = useColorModeValue("gray.600", "whiteAlpha.700");

  return (
    <VStack
      d="flex"
      flexDir="column"
      flex="1"
      color={textColor}
      zIndex={50}
      px="4"
      py="2"
      spacing="8"
      mb="8"
    >
      <ScreenTitle title="Skills" />
      <VStack spacing="8">
        {data.skills.map((skill, index) => (
          <SkillCard skill={skill} key={`skill-${index}`} />
        ))}
      </VStack>
    </VStack>
  );
}
