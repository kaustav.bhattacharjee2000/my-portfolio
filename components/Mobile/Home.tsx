import React from "react";
import {
  Flex,
  Box,
  Square,
  Center,
  VStack,
  Text,
  Icon,
  Link,
  Image,
  Button,
  Spacer,
  useClipboard,
  useColorModeValue,
} from "@chakra-ui/react";
import { ExternalLinkIcon, ChevronRightIcon } from "@chakra-ui/icons";

import type { LinkBoxProps } from "../../utils/types";
import CustomLink from "../Common/CustomLink";
import ScreenTitle from "../Common/ScreenTitle";
import { data } from "../../utils/data";

function LinkBox(props: LinkBoxProps) {
  const { hasCopied, onCopy } = useClipboard(props.url);

  const borderColor = useColorModeValue("gray.300", "inherit");
  const iconWrapperBg = useColorModeValue(
    props.iconBgColor[props.iconBgColor.length - 1],
    props.iconBgColor[0]
  );
  const iconColor = useColorModeValue(
    props.iconColor[props.iconColor.length - 1],
    props.iconColor[0]
  );

  return (
    <Flex
      w="full"
      border="1px"
      borderColor={borderColor}
      px="3"
      py="2"
      rounded="xl"
      alignItems="center"
    >
      <Square bg={iconWrapperBg} size="40px" rounded="lg">
        <Icon
          as={props.icon}
          w={props.iconSize}
          h={props.iconSize}
          color={iconColor}
        />
      </Square>

      <Box flex="1" ml="4">
        <Flex alignItems="center">
          <Text fontWeight="bold">{props.title}</Text>
          <Spacer />
          <Button size="sm" onClick={onCopy}>
            {hasCopied ? "Copied" : "Copy"}
          </Button>
        </Flex>
        <Text fontSize="14" isTruncated>
          {props.subtitle}
        </Text>
      </Box>
    </Flex>
  );
}

export default function Home() {
  const textColor = useColorModeValue("gray.600", "whiteAlpha.700");
  const nameColor = useColorModeValue("inherit", "white");
  const hiThereColor = useColorModeValue("red.300", "white");

  return (
    <VStack
      d="flex"
      flexDir="column"
      flex="1"
      color={textColor}
      zIndex={50}
      px="4"
      py="2"
      spacing="10"
    >
      <ScreenTitle title="About Me" />
      <Box flex="1">
        <Center flexDir="column">
          <Square
            d="flex"
            alignItems="flex-end"
            size="32"
            border="2px"
            borderColor="gray.300"
            rounded="3xl"
            overflow="hidden"
          >
            <Image
              src={data.profilePic}
              alt="My picture"
              htmlWidth={"128px"}
              htmlHeight={"120px"}
            />
          </Square>
          <Text mt="4" fontSize="lg" fontWeight="bold" color={nameColor}>
            Kaustav Bhattacharjee
          </Text>
          <Text>Software Developer</Text>
        </Center>

        <Box mt="8">
          <Text fontSize="xl" fontWeight="bold" color={hiThereColor}>
            Hi there !
          </Text>
          <Text fontSize="lg" align="justify">
            {data.aboutMe}
          </Text>

          <VStack mt="12" mb="8" spacing="4">
            {data.links.map((link, index) => (
              <LinkBox key={`mylink-${index}`} {...link} />
            ))}
          </VStack>
        </Box>

        <Center mb="12">
          <Link
            href={data.resume}
            _hover={{ textDecoration: "none" }}
            isExternal
          >
            <Button color="white" bg="red.300">
              Download my resume
            </Button>
          </Link>
        </Center>

        <VStack mb="10" spacing={8}>
          <Center>
            <CustomLink internalLink="/lofi">
              <Text>Listen to Lo-fi music</Text>
              <ChevronRightIcon boxSize="6" />
            </CustomLink>
          </Center>

          <Center>
            <CustomLink externalLink={data.repo}>
              <Text>GitLab Repo</Text>
              <ExternalLinkIcon ml="2" />
            </CustomLink>
          </Center>
        </VStack>
      </Box>
    </VStack>
  );
}
