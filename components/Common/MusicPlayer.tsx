import React, { useState, useEffect, useCallback } from "react";
import { AudioPlayerProvider } from "react-use-audio-player";
import { useAudioPlayer, useAudioPosition } from "react-use-audio-player";

import type { Playlist, BaseLayoutProps, Track } from "../../utils/types";
import tracks from "../../utils/songs";

interface TrackPlayer extends BaseLayoutProps {
  track: Track;
  toPrevTrack: Function;
  toNextTrack: Function;
}

// Randomize array in-place using Durstenfeld shuffle algorithm
const getShuffledTracks = () => {
  // traverse from end of array
  for (let fromEnd = tracks.length - 1; fromEnd > 0; fromEnd--) {
    // pick a random index
    const randomIndex = Math.floor(Math.random() * (fromEnd + 1));
    // swap the values of current index (from end) and random index
    [tracks[fromEnd], tracks[randomIndex]] = [
      tracks[randomIndex],
      tracks[fromEnd],
    ];
  }

  return tracks;
};

function SingleTrackPlayer({
  track,
  toNextTrack,
  toPrevTrack,
  page: Page,
}: TrackPlayer) {
  const { togglePlayPause, ready, playing } = useAudioPlayer({
    src: track.audio,
    format: "mp3",
    autoplay: true,
    onend: toNextTrack,
  });

  const { position, duration, seek } = useAudioPosition({
    highRefreshRate: true,
  });

  const displayTimeFormat = (totalSeconds: number) => {
    let minutes = Math.floor(totalSeconds / 60);
    let seconds = Math.floor(totalSeconds - minutes * 60);

    return `${minutes}:${String(seconds).padStart(2, "0")}`;
  };

  const audioTimes = useCallback(() => {
    let spentTime = displayTimeFormat(position);
    let remainingTime = displayTimeFormat(duration ? duration - position : 0);
    let totalTime = displayTimeFormat(duration ? duration : 0);

    return { spentTime, remainingTime, totalTime };
  }, [duration, position]);

  const onScrub = useCallback(
    (value) => {
      console.log("onScrub value => ", value);
      seek(value);
    },
    [duration, seek]
  );

  return (
    <Page
      track={track}
      trackReady={ready}
      audioControls={{
        isPlaying: playing,
        onPrevClick: toPrevTrack,
        onNextClick: toNextTrack,
        onPlayPauseClick: togglePlayPause,
      }}
      playbackSlider={{
        duration,
        trackProgress: position,
        onScrub: onScrub,
        audioTimes,
      }}
    />
  );
}

export default function MusicPlayer({ page }: BaseLayoutProps) {
  const [tracks, setTracks] = useState<Playlist>([]);
  const [trackIndex, setTrackIndex] = useState(0);

  useEffect(() => {
    if (tracks.length === 0) {
      setTracks(getShuffledTracks());
    }
  }, []);

  const toPrevTrack = () => {
    if (trackIndex - 1 < 0) {
      setTrackIndex(tracks.length - 1);
    } else {
      setTrackIndex(trackIndex - 1);
    }
  };

  const toNextTrack = () => {
    if (trackIndex < tracks.length - 1) {
      setTrackIndex(trackIndex + 1);
    } else {
      setTrackIndex(0);
    }
  };

  return typeof tracks[trackIndex] !== "undefined" ? (
    <AudioPlayerProvider>
      <SingleTrackPlayer
        page={page}
        track={tracks[trackIndex]}
        toPrevTrack={toPrevTrack}
        toNextTrack={toNextTrack}
      />
    </AudioPlayerProvider>
  ) : null;
}
