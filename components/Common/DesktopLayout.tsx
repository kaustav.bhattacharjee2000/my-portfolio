import React from "react";
import { Flex, Center } from "@chakra-ui/react";

import Sidebar from "./Sidebar";
import CustomScrollbar from "./CustomScrollbar";

const DesktopLayout: React.FC<{}> = ({ children }) => {
  return (
    <Flex flex="1">
      <Sidebar />

      <CustomScrollbar marginLeft="250px">
        <Center>
          <Flex w="full" maxW={"900px"} h="full">
            {children}
          </Flex>
        </Center>
      </CustomScrollbar>
    </Flex>
  );
};

export default DesktopLayout;
