import React from "react";
import { Flex, Spacer, Text, useColorMode } from "@chakra-ui/react";
import { DarkModeSwitch } from "react-toggle-dark-mode";

interface ScreenTitleProps {
  title: string;
  titleColor?: string;
  toggleColor?: string;
  noToggle?: boolean;
}

export default function ScreenTitle(props: ScreenTitleProps) {
  const { toggleColorMode, colorMode } = useColorMode();
  const defaultTitleColor = colorMode === "dark" ? "red.300" : "inherit";

  return (
    <Flex w="full" alignItems="center">
      <Text
        fontSize="2xl"
        fontWeight="medium"
        color={props.titleColor || defaultTitleColor}
      >
        {props.title}
      </Text>

      <Spacer />

      {!props.noToggle && (
        <DarkModeSwitch
          checked={colorMode === "dark"}
          onChange={toggleColorMode}
          sunColor={props.toggleColor || "#F28C38"}
        />
      )}
    </Flex>
  );
}
