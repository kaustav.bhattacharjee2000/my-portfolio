import React from "react";
import NextLink from "next/link";
import { Flex, Link } from "@chakra-ui/react";

interface CustomLinkProps {
  externalLink?: string;
  internalLink?: string;
}

const CustomLink: React.FC<CustomLinkProps> = ({
  externalLink,
  internalLink,
  children,
}) => {
  return !!externalLink ? (
    <Link href={externalLink!} isExternal>
      <Flex alignItems={"center"}>{children}</Flex>
    </Link>
  ) : (
    <NextLink href={internalLink!} passHref>
      <Link>
        <Flex alignItems={"center"}>{children}</Flex>
      </Link>
    </NextLink>
  );
};

export default CustomLink;
