import "simplebar/dist/simplebar.min.css";
import React from "react";
import SimpleBar from "simplebar-react";

import useWindowDimentions from "../../hooks/useWindowDimentions";

interface CustomScrollbarProps {
  marginLeft: string;
}

const CustomScrollbar: React.FC<CustomScrollbarProps> = ({
  marginLeft,
  children,
}) => {
  const { height } = useWindowDimentions();

  return (
    <SimpleBar
      style={{ maxHeight: height, width: "100%", marginLeft: marginLeft }}
    >
      {children}
    </SimpleBar>
  );
};

export default CustomScrollbar;
