import React from "react";
import dynamic from "next/dynamic";
import { Flex, useBreakpointValue } from "@chakra-ui/react";

import type { BaseLayoutProps } from "../../utils/types";

const MobileLayout = dynamic(() => import("./MobileLayout"));
const DesktopLayout = dynamic(() => import("./DesktopLayout"));

export default function BaseLayout({ page: Page }: BaseLayoutProps) {
  const displayMobileView = useBreakpointValue({ base: true, lg: false });

  return (
    <Flex minH="100vh" minW="100vw">
      {displayMobileView !== undefined &&
        displayMobileView !== null &&
        (displayMobileView ? (
          <MobileLayout>
            <Page displayMobileView={displayMobileView} />
          </MobileLayout>
        ) : (
          <DesktopLayout>
            <Page displayMobileView={displayMobileView} />
          </DesktopLayout>
        ))}
    </Flex>
  );
}
