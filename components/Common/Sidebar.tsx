import React from "react";
import {
  Flex,
  Box,
  VStack,
  Text,
  Icon,
  Center,
  useColorModeValue,
  useColorMode,
} from "@chakra-ui/react";
import { ExternalLinkIcon, ChevronRightIcon } from "@chakra-ui/icons";
import { DarkModeSwitch } from "react-toggle-dark-mode";
import { useRouter } from "next/router";
import { FaUserCircle } from "react-icons/fa";
import { HiAcademicCap, HiArchive } from "react-icons/hi";
import { IoBuild } from "react-icons/io5";

import type { ChakraIconType } from "../../utils/types";
import CustomLink from "./CustomLink";
import { data } from "../../utils/data";

interface SidebarItemProps {
  name: string;
  to: string;
  icon: ChakraIconType;
}

function Title() {
  return (
    <Text mx="6" fontSize="3xl" mt="2" mb="6" color="red.300">
      Portfolio
    </Text>
  );
}

function SidebarItem(props: SidebarItemProps) {
  const router = useRouter();

  const isSameRoute = router.pathname === props.to;

  const navigate = () => {
    if (!isSameRoute) {
      router.push(props.to);
    }
  };

  return (
    <Flex
      align="center"
      py="4"
      pl="4"
      pr="20"
      mx="2"
      rounded="lg"
      cursor="pointer"
      color={isSameRoute ? "red.300" : "inherit"}
      _hover={{ bg: "red.300", color: "white" }}
      onClick={navigate}
    >
      <Icon w="6" h="6" as={props.icon} mr="4" />
      <Text fontWeight={isSameRoute ? "bold" : "normal"}>{props.name}</Text>
    </Flex>
  );
}

function ColorModeChanger() {
  const { toggleColorMode, colorMode } = useColorMode();
  const textColor = colorMode === "light" ? "inherit" : "gray.300";
  const borderColor = colorMode === "light" ? "gray.300" : "gray.500";

  return (
    <Flex
      align="center"
      justify="space-between"
      p="4"
      mx="2"
      mt="10"
      rounded="lg"
      border="1px"
      borderColor={borderColor}
    >
      <Text as="strong" color={textColor}>
        Background mode
      </Text>
      <DarkModeSwitch
        checked={colorMode === "dark"}
        onChange={toggleColorMode}
        sunColor="#F28C38"
      />
    </Flex>
  );
}

export default function Sidebar() {
  const borderColor = useColorModeValue("gray.200", "transparent");
  const bg = useColorModeValue("white", "blackAlpha.400");

  return (
    <Flex
      pos="fixed"
      left="0"
      h="full"
      borderRight="2px"
      borderRightColor={borderColor}
      py="4"
      px="2"
      flexDir="column"
      justify="space-between"
      color="gray.500"
      bg={bg}
    >
      <Box>
        <Title />
        <SidebarItem name="About Me" icon={FaUserCircle} to="/" />
        <SidebarItem name="Experience" icon={HiAcademicCap} to="/experience" />
        <SidebarItem name="Skills" icon={HiArchive} to="/skills" />
        <SidebarItem name="Projects" icon={IoBuild} to="/projects" />
        <ColorModeChanger />
      </Box>

      <VStack spacing={4}>
        <Center>
          <CustomLink internalLink="/lofi">
            <Text>Listen to Lo-fi music</Text>
            <ChevronRightIcon boxSize="6" />
          </CustomLink>
        </Center>

        <Center>
          <CustomLink externalLink={data.repo}>
            <Text>GitLab Repo</Text>
            <ExternalLinkIcon ml="2" />
          </CustomLink>
        </Center>
      </VStack>
    </Flex>
  );
}
