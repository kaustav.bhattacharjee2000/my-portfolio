import React from "react";
import dynamic from "next/dynamic";
import { Flex, Text, Center } from "@chakra-ui/react";

import type { PageProps } from "../../utils/types";

interface MiscPageProps extends PageProps {
  title: string;
}

const ScreenTitle = dynamic(() => import("./ScreenTitle"));

const MiscPage: React.FC<MiscPageProps> = ({
  displayMobileView,
  title,
  children,
}) => {
  return (
    <Flex h="100vh" flexDir="column" flex="1" px="4" py="2">
      {displayMobileView && <ScreenTitle title="" />}

      <Center flex="1">
        <Flex maxW="420px" flexDir="column" align="center" justify="center">
          <Text
            bgGradient="linear(to-l, #7928CA,#FF0080)"
            bgClip="text"
            fontSize="4xl"
            fontWeight="extrabold"
            mb="4"
          >
            {title}
          </Text>

          {children}
        </Flex>
      </Center>
    </Flex>
  );
};

export default MiscPage;
