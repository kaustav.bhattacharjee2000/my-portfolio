import React, { Fragment } from "react";
import { Flex, Box } from "@chakra-ui/react";

import BottomNavigation from "./BottomNavigation";

const MobileLayout: React.FC<{}> = ({ children }) => {
  return (
    <Fragment>
      <Flex flex="1" mb="60px">
        {children}
      </Flex>

      <Box w="full" pos="fixed" bottom="0" zIndex="999">
        <BottomNavigation />
      </Box>
    </Fragment>
  );
};

export default MobileLayout;
