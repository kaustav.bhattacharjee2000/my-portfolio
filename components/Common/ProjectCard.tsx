import React from "react";
import NextImage from "next/image";
import {
  Box,
  Heading,
  HStack,
  Tag,
  Badge,
  Flex,
  Text,
  Button,
  Collapse,
  Center,
  Link,
  useBoolean,
  useColorModeValue,
} from "@chakra-ui/react";

import type { Project, ProjectType } from "../../utils/types";

import followmeImage from "../../public/assets/followme.jpg";
import dropnextImage from "../../public/assets/dropnext.jpg";
import rosterImage from "../../public/assets/roster.jpg";
import save2qrImage from "../../public/assets/save2qr.jpg";
import pmsImage from "../../public/assets/pms.jpg";
import cakebotImage from "../../public/assets/cakebot.jpg";
import icImage from "../../public/assets/ic.jpg";

interface ProjectCardProps {
  project: Project;
  isMobile?: boolean;
}

const projectTypeToColorMapping: Record<ProjectType, string> = {
  "Front End": "red",
  "Back End": "purple",
  Mobile: "orange",
  Desktop: "orange",
  "Machine Learning": "orange",
};

const projectImageMapping: Record<string, StaticImageData> = {
  "/assets/followme.jpg": followmeImage,
  "/assets/dropnext.jpg": dropnextImage,
  "/assets/roster.jpg": rosterImage,
  "/assets/save2qr.jpg": save2qrImage,
  "/assets/pms.jpg": pmsImage,
  "/assets/cakebot.jpg": cakebotImage,
  "/assets/ic.jpg": icImage,
};

export default function ProjectCard({ project, isMobile }: ProjectCardProps) {
  const [showFull, { toggle: toggleShowFull }] = useBoolean(false);

  const headingTextColor = useColorModeValue("inherit", "white");
  const showTextColor = useColorModeValue("inherit", "teal.300");
  const linkBtnColor = useColorModeValue("teal", "red");
  const bgColor = useColorModeValue("white", "gray.700");

  const paddingY = isMobile ? 0 : 6;
  const maxW = isMobile ? "full" : "360px";
  const boxShadow = isMobile ? "none" : "2xl";
  const rounded = isMobile ? "xl" : "md";
  const headingMt = isMobile ? 4 : 6;
  const contentP = isMobile ? 3 : 6;
  const contentMt = isMobile ? 2 : 0;
  const linkTextSize = isMobile ? "xs" : "sm";
  const linkTextMt = isMobile ? 4 : 6;

  return (
    <Center py={paddingY}>
      <Box
        maxW={maxW}
        w="full"
        h="full"
        border="1px"
        boxShadow={boxShadow}
        borderColor="gray.300"
        rounded={rounded}
        overflow="hidden"
        bg={bgColor}
      >
        <Box
          h={"210px"}
          maxH={"500px"}
          pos={"relative"}
          borderBottom="1px"
          borderColor="gray.300"
        >
          <NextImage
            src={projectImageMapping[project.image]}
            layout={"fill"}
            objectFit="cover"
            alt={`${project.title} demo image`}
          />
        </Box>

        <Box mt={contentMt} p={contentP}>
          <HStack>
            {project.projectType.map((name) => (
              <Badge
                key={`${project.title}-${name}`}
                colorScheme={projectTypeToColorMapping[name]}
              >
                {name}
              </Badge>
            ))}
          </HStack>

          <Center mt={headingMt} mb="4">
            <Heading fontSize="xl" color={headingTextColor}>
              {project.title}
            </Heading>
          </Center>

          <Collapse startingHeight={40} in={showFull} animateOpacity>
            <Text>{project.description}</Text>

            <Heading size="xs" mt="4">
              Tech Stacks
            </Heading>

            <Box my="1">
              {project.techStack.map((name) => (
                <Tag m="1" key={`${project.title}-${name}`}>
                  {name}
                </Tag>
              ))}
            </Box>
          </Collapse>

          <Flex>
            <Text
              color={showTextColor}
              as="strong"
              cursor="pointer"
              onClick={toggleShowFull}
            >
              Show {showFull ? "less" : "more"}
            </Text>
          </Flex>

          <Heading size={linkTextSize} mt={linkTextMt}>
            Links
          </Heading>

          <HStack mt="1">
            {project.links.map((name) => (
              <Link
                key={name.title}
                href={name.url}
                _hover={{ textDecoration: "none" }}
                isExternal
              >
                <Button
                  colorScheme={linkBtnColor}
                  size="sm"
                  mr="1"
                  mt="1"
                  key={`${project.title}-${name.title}`}
                >
                  {name.title}
                </Button>
              </Link>
            ))}
          </HStack>
        </Box>
      </Box>
    </Center>
  );
}
