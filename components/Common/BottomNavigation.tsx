import React from "react";
import { useRouter } from "next/router";
import {
  SimpleGrid,
  Flex,
  Icon,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import { FaUserCircle } from "react-icons/fa";
import { HiAcademicCap, HiArchive } from "react-icons/hi";
import { IoBuild } from "react-icons/io5";

import type { ChakraIconType } from "../../utils/types";

interface TabProps {
  icon: ChakraIconType;
  name: string;
  to: string;
}

function NavigationTab(props: TabProps) {
  const router = useRouter();

  const isSameRoute = router.pathname === props.to;

  const navigate = () => {
    if (!isSameRoute) {
      router.push(props.to);
    }
  };

  return (
    <Flex
      p="2"
      flexDir="column"
      alignItems="center"
      color={isSameRoute ? "red.300" : "inherit"}
      onClick={navigate}
    >
      <Icon as={props.icon} w="6" h="6" />
      <Text fontSize="sm" fontWeight={isSameRoute ? "bold" : "normal"}>
        {props.name}
      </Text>
    </Flex>
  );
}

export default function BottomNavigation() {
  const bgColor = useColorModeValue("white", "gray.900");

  return (
    <SimpleGrid
      color="gray.500"
      h="max-content"
      w="full"
      columns={4}
      borderTop="1px"
      borderTopColor="gray.300"
      roundedTop="3xl"
      bg={bgColor}
    >
      <NavigationTab name="About" icon={FaUserCircle} to="/" />
      <NavigationTab name="Experience" icon={HiAcademicCap} to="/experience" />
      <NavigationTab name="Skills" icon={HiArchive} to="/skills" />
      <NavigationTab name="Projects" icon={IoBuild} to="/projects" />
    </SimpleGrid>
  );
}
