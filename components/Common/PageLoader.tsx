import React from "react";
import { Center, Spinner } from "@chakra-ui/react";

export default function PageLoader() {
  return (
    <Center w="full" h="calc(100vh - 60px)">
      <Spinner
        thickness="4px"
        speed="0.65s"
        emptyColor="gray.200"
        color="red.300"
        size="xl"
      />
    </Center>
  );
}
