/**
 * Common types used across pages
 */

import type { ElementType, MouseEventHandler } from "react";
import type { ComponentWithAs } from "@chakra-ui/system";
import type { IconProps } from "@chakra-ui/icons";
import type { IconType } from "react-icons";

export type ChakraIconType = ComponentWithAs<"svg", IconProps> | IconType;

export type ColorModeType = Array<string>; // [lightMode, darkMode]

export interface ExperienceCardProps {
  position: string;
  company: string;
  description: string;
  timeline: Array<string>;
  website: string;
  isMobile?: boolean;
}

export interface LinkBoxProps {
  title: string;
  subtitle: string;
  icon: ChakraIconType;
  url: string;
  iconSize: number;
  iconBgColor: ColorModeType;
  iconColor: ColorModeType;
}

export type ProjectLinkType = {
  url: string;
  title: string;
};

export type ProjectType =
  | "Front End"
  | "Back End"
  | "Mobile"
  | "Desktop"
  | "Machine Learning";

export interface Project {
  title: string;
  description: string;
  projectType: Array<ProjectType>;
  techStack: Array<string>;
  links: Array<ProjectLinkType>;
  image: string;
}

export interface Skill {
  title: string;
  tags: Array<string>;
  icon: ChakraIconType;
  image: string;
}

export interface PageProps {
  displayMobileView: boolean;
}

export interface BaseLayoutProps {
  page: ElementType;
}

export interface Track {
  name: string;
  cover: string;
  artist: string;
  audio: string;
  id: number;
}

export type Playlist = Array<Track>;

export interface AudioControlsProps {
  isPlaying: boolean;
  onPlayPauseClick: Function;
  onPrevClick: MouseEventHandler;
  onNextClick: MouseEventHandler;
}

export interface PlaybackSliderProps {
  duration: number;
  trackProgress: number;
  onScrub: Function;
  audioTimes: Function;
}

export interface LofiScreenProps {
  track: Track;
  trackReady: boolean;
  audioControls: AudioControlsProps;
  playbackSlider: PlaybackSliderProps;
}

export interface PortfolioData {
  links: Array<LinkBoxProps>;
  experiences: Array<ExperienceCardProps>;
  projects: Array<Project>;
  skills: Array<Skill>;
  resume: string;
  repo: string;
  profileBackground: string;
  lofiBackground: string;
  aboutMe: string;
  profilePic: string;
}
