import dynamic from "next/dynamic";

import PageLoader from "../components/Common/PageLoader";

export default function dynamicPage(path: any) {
  return dynamic(path, { loading: PageLoader });
}
