import { Playlist } from "../utils/types";
import SongsData from "./songs.json";

const songs: Playlist = SongsData;

export default songs;
