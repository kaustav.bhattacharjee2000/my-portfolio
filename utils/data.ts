// @types
import type { ProjectType, ChakraIconType, PortfolioData } from "./types";

// @icons
import {
  AiFillGithub,
  AiOutlineGitlab,
  AiOutlineTwitter,
} from "react-icons/ai";
import {
  FaLinkedinIn,
  FaFacebookF,
  FaServer,
  FaDatabase,
} from "react-icons/fa";
import { SiGmail, SiSmartthings } from "react-icons/si";
import { CgWebsite } from "react-icons/cg";
import { ImMobile } from "react-icons/im";
import { BsGearWideConnected } from "react-icons/bs";

import details from "../details.json";

export const socialIconsMapping: Record<string, ChakraIconType> = {
  Gmail: SiGmail,
  LinkedIn: FaLinkedinIn,
  GitHub: AiFillGithub,
  GitLab: AiOutlineGitlab,
  Twitter: AiOutlineTwitter,
  Facebook: FaFacebookF,
};

export const skillIconsMapping: Record<string, ChakraIconType> = {
  "Front End": CgWebsite,
  "Back End": FaServer,
  Database: FaDatabase,
  Mobile: ImMobile,
  "Machine Learning": SiSmartthings,
  Others: BsGearWideConnected,
};

export const data: PortfolioData = {
  ...details,
  links: details.links.map((value) => ({
    ...value,
    icon: socialIconsMapping[value.title],
  })),
  skills: details.skills.map((value) => ({
    ...value,
    icon: skillIconsMapping[value.title],
  })),
  projects: details.projects.map((value) => ({
    ...value,
    projectType: value.projectType as ProjectType[],
  })),
};
